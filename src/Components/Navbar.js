import React, { useState } from "react";
import { Link } from "react-router-dom";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import IconButton from "@mui/material/IconButton";
export default function Navbar() {
  const [darkMode, setDarkMode] = useState(false);
  const changeTheme = () => {
    document.body.classList.toggle("dark");
    setDarkMode(!darkMode);
  };
  return (
    <div className="w-full h-20">
      <div className="max-w-7xl mx-auto flex flex-row justify-between items-center p-6">
        <Link to="/">
          <h2 className="font-bold text-xl text-white ">Crypto</h2>
        </Link>
        <div className="flex flex-row justify-center item-center gap-x-5">
          <nav className="flex flex-row justify-center items-center gap-x-2 text-white">
            <a href="#" className="">
              Home
            </a>
            <a href="#" className="">
              About
            </a>
            <a href="#" className="">
              Services
            </a>
            <a href="#" className="">
              Contact
            </a>
          </nav>
          <IconButton>
            <DarkModeIcon
              onClick={changeTheme}
              style={{ fontSize: 20 }}
              sx={{ color: "white" }}
            />
          </IconButton>
        </div>
      </div>
    </div>
  );
}
