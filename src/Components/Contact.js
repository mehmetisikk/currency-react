import React from "react";
import TelegramIcon from "@mui/icons-material/Telegram";
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import GitHubIcon from "@mui/icons-material/GitHub";
import IconButton from "@mui/material/IconButton";

const Contact = () => {
  return (
    <div className="absolute bg-[#000272] top-1/3 left-[20px] z-10 rounded-full p-3 hidden md:block">
      <nav className="flex flex-col justify-center items-center gap-y-5">
        <IconButton>
          <TelegramIcon style={{ fontSize: 30 }} sx={{ color: "white" }} />
        </IconButton>
        <IconButton>
          <FacebookIcon style={{ fontSize: 30 }} sx={{ color: "white" }} />
        </IconButton>
        <IconButton>
          <TwitterIcon style={{ fontSize: 30 }} sx={{ color: "white" }} />
        </IconButton>
        <IconButton>
          <GitHubIcon style={{ fontSize: 30 }} sx={{ color: "white" }} />
        </IconButton>
        <IconButton>
          <InstagramIcon style={{ fontSize: 30 }} sx={{ color: "white" }} />
        </IconButton>
        <IconButton>
          <LinkedInIcon style={{ fontSize: 30 }} sx={{ color: "white" }} />
        </IconButton>
      </nav>
    </div>
  );
};

export default Contact;
