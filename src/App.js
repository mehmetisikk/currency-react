import React, { useState, useEffect } from "react";
import HomePage from "./Pages/HomePage";
import Table from "./Pages/Table";
import ChartPage from "./Pages/ChartPage";
import Context from "./Context";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
function App() {
  const [data, setData] = useState([]);
  const [selected, setSelected] = useState();
  const [selectedCoins, setSelectedCoins] = useState([]);

  useEffect(() => {
    fetch("https://api.coinstats.app/public/v1/coins?&currency=USD")
      .then((res) => res.json())
      .then((data) => setData(data.coins));
  }, []);
  const Contextdata = {
    data,
    setData,
    selected,
    setSelected,
    selectedCoins,
    setSelectedCoins,
  };
  return (
    <div className="App min-h-screen w-full">
      <Context.Provider value={Contextdata}>
        <Router>
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/table" element={<Table />} />
            <Route path="/chart" element={<ChartPage />} />
          </Routes>
        </Router>
      </Context.Provider>
    </div>
  );
}

export default App;
