import React, { useEffect, useRef } from "react";
import image from "../Images/banner-bg-3df4d356ab06a65d5f45a74b341a1eac.png";
import Navbar from "../Components/Navbar";
import Contact from "../Components/Contact";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import lottie from "lottie-web";
const HomePage = () => {
  return (
    <div className="HomePage w-full min-h-screen relative dark:bg-black">
      <Navbar />
      <div className=" max-w-7xl  h-[calc(100vh-5rem)] mx-auto flex justify-center items-center ">
        <div className="flex flex-row justify-center items-center gap-y-2">
          <div className="flex-1 flex flex-col justify-center items-center gap-y-12 m-10">
            <span className="px-3 py-2 rounded text-white  bg-slate-600 opacity-75">
              Discover the Crypto currency world
            </span>
            <h2 className="text-5xl font-bold text-white">
              Crypto <span className="text-red-600">Currency</span>
            </h2>
            <p className="text-white text-xl">
              Lorem Ipsum is simply dummy text
              <br /> of the printing and typesetting industry.
            </p>
            <Link to="/table">
              <Button variant="contained">Explore</Button>
            </Link>
          </div>
        </div>
      </div>
      <Contact />
    </div>
  );
};

export default HomePage;
