import React, { useState, useContext } from "react";
import Context from "../Context";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Avatar from "@material-ui/core/Avatar";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import TableRow from "@material-ui/core/TableRow";
import TableContainer from "@material-ui/core/TableContainer";
import Grid from "@mui/material/Grid";
import Checkbox from "@mui/material/Checkbox";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Navbar from "../Components/Navbar";
import { useNavigate } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import Pagination from "@material-ui/lab/Pagination";

const TablePage = (id) => {
  const navigate = useNavigate();
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);

  const {
    data,
    setData,
    selected,
    setSelected,
    selectedCoins,
    setSelectedCoins,
  } = useContext(Context);
  const selectHandle = (id) => {
    if (selectedCoins.includes(id)) {
      setSelectedCoins(selectedCoins.filter((coin) => coin !== id));
    } else {
      setSelectedCoins([...selectedCoins, id]);
    }
  };
  const filterData = () => {
    return data.filter((coin) =>
      coin.name.toLowerCase().includes(search.toLowerCase())
    );
  };
  const handleChart = () => {
    navigate("/chart", { state: selectedCoins });
  };
  return (
    <div className="w-full min-h-screen relative bg-[#03045e]">
      <Navbar />
      <div className="max-w-7xl mx-auto h-full flex flex-col p-6 gap-y-12">
        <div className="flex flex-row justify-between ">
          <h2 className="text-white">Table</h2>
          <div className="flex flex-row justify-center item-center gap-x-12">
            <TextField
              color="secondary"
              className={"text-white border-white"}
              label="Search Coin"
              variant="outlined"
              onChange={(e) => setSearch(e.target.value)}
            ></TextField>
            <button
              className="text-white border border-white px-6"
              onClick={() => handleChart()}
            >
              GO TO COMPARE SELECTED COINS {selectedCoins.length}
            </button>
          </div>
        </div>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                {[
                  "#",
                  "NAME",
                  "24H CHANGE",
                  "PRICE",
                  "PRICE IN BTC",
                  "MARKET CAP",
                  "VOLUME 24H",
                ].map((head) => (
                  <TableCell
                    style={{
                      color: "black",
                      fontWeight: "bold",
                      textAlign: "left",
                      whiteSpace: "nowrap",
                    }}
                    key={head}
                    align={head === "NAME" || head === "#" ? "" : "right"}
                  >
                    {head}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {filterData()
                .slice((page - 1) * 10, (page - 1) * 10 + 10)
                .map(
                  ({
                    id,
                    icon,
                    name,
                    priceChange1d,
                    price,
                    priceBtc,
                    marketCap,
                    volume,
                  }) => (
                    <TableRow key={id}>
                      <TableCell>
                        <Checkbox
                          onChange={() => selectHandle(id)}
                          inputProps={{ "aria-label": "controlled" }}
                        />
                      </TableCell>
                      <TableCell component="th">
                        <Grid
                          container
                          direction="row"
                          alignItems="center"
                          gap={3}
                        >
                          <Grid item>
                            <Avatar alt="Remy Sharp" src={icon} />
                          </Grid>
                          <Grid item>{name}</Grid>
                        </Grid>
                      </TableCell>
                      <TableCell numeric>
                        {priceChange1d > 0 ? (
                          <Box
                            sx={{
                              flexDirection: "row",
                              alignItems: "center",
                              justifyContent: "center",
                            }}
                          >
                            <ExpandLessIcon color="success" />
                            {priceChange1d}
                          </Box>
                        ) : (
                          <Box sx={{ flexDirection: "row" }}>
                            <ExpandMoreIcon sx={{ color: "red" }} />
                            {priceChange1d}
                          </Box>
                        )}
                      </TableCell>
                      <TableCell numeric>{price.toFixed(2)}</TableCell>
                      <TableCell numeric>{priceBtc}</TableCell>
                      <TableCell numeric>{marketCap.toFixed(1)}</TableCell>
                      <TableCell numeric>{volume}</TableCell>
                    </TableRow>
                  )
                )}
            </TableBody>
          </Table>
        </TableContainer>
        <Pagination
          color="secondary"
          count={(filterData()?.length / 10).toFixed(0)}
          style={{
            padding: 20,
            width: "100%",
            display: "flex",
            justifyContent: "center",
          }}
          onChange={(_, value) => {
            setPage(value);
            window.scroll(0, 450);
          }}
        />
      </div>
    </div>
  );
};

export default TablePage;
