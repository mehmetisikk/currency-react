import axios from "axios";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import TabContext from "@material-ui/lab/TabContext";
import TabList from "@material-ui/lab/TabList";
import TabPanel from "@material-ui/lab/TabPanel";
import { Line, Bar } from "react-chartjs-2";
import Navbar from "../Components/Navbar";
import {
  CircularProgress,
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,

  Title,
  Tooltip,
  Legend
);

const ChartPage = () => {
  const [historicData, setHistoricData] = useState();
  const [days, setDays] = useState("1m");
  const [flag, setflag] = useState(false);
  const [newStates, setnewStates] = useState();

  const { state } = useLocation();

  //Tabs
  const [tabsValue, settabsValue] = useState("1");

  const handleTabChange = (event, newValue) => {
    settabsValue(newValue);
  };
  // get days from user
  const handleChange = (event) => {
    setDays(event.target.value);
  };

  //fetch data
  const fetchHistoricData = async () => {
    const { data } = await axios.get(
      `https://api.coinstats.app/public/v1/charts?period=${days}&coinId=${state[0]}`
    );
    setflag(true);
    setHistoricData(data.chart);
  };

  //useEffect
  useEffect(() => {
    fetchHistoricData();
  }, [days]);
  const darkTheme = createTheme({
    palette: {
      primary: {
        main: "#fff",
      },
      type: "dark",
    },
  });
  return (
    <div className="w-full min-h-screen relative bg-[#ccdbfd] dark:bg-[#03045e]">
      <div className="max-w-7xl mx-auto h-full">
        <Navbar />
        <TabContext value={tabsValue}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList
              onChange={handleTabChange}
              aria-label="lab API tabs example"
            >
              <Tab label="Line Graph" value="1" />
              <Tab label="Bar Graph" value="2" />
            </TabList>
          </Box>

          <TabPanel value="1">
            {!historicData | (flag === false) ? (
              <CircularProgress
                style={{ color: "gold" }}
                size={250}
                thickness={1}
              />
            ) : (
              <Line
                data={{
                  labels: historicData.map((coin) => {
                    let date = new Date(coin[0]);
                    let time =
                      date.getHours() > 12
                        ? `${date.getHours() - 12}:${date.getMinutes()} PM`
                        : `${date.getHours()}:${date.getMinutes()} AM`;
                    return days === 1 ? time : date.toLocaleDateString();
                  }),
                  datasets: [
                    {
                      data: historicData.map((coin) => coin[1]),
                      label: `Price ( Past ${days} Days )`,
                      borderColor: "#EEBC1D",
                    },
                    {
                      data: historicData.map((coin) => coin[2]),
                      label: `Price ( Past ${days} Days )`,
                      borderColor: "#EEBC1D",
                    },
                  ],
                }}
                options={{
                  elements: {
                    point: {
                      radius: 1,
                    },
                  },
                }}
              />
            )}
          </TabPanel>
          <TabPanel value="2">
            {/* <Bar
            data={{
              labels: historicData.map((coin) => {
                let date = new Date(coin[0]);
                let time =
                  date.getHours() > 12
                    ? `${date.getHours() - 12}:${date.getMinutes()} PM`
                    : `${date.getHours()}:${date.getMinutes()} AM`;
                return days === 1 ? time : date.toLocaleDateString();
              }),
              datasets: [
                {
                  data: historicData.map((coin) => coin[1]),
                  label: `Price ( Past ${days} Days )`,
                  borderColor: "#EEBC1D",
                },
                {
                  data: historicData.map((coin) => coin[2]),
                  label: `Price ( Past ${days} Days )`,
                  borderColor: "#EEBC1D",
                },
              ],
            }}
            options={{
              elements: {
                point: {
                  radius: 1,
                },
              },
            }}
          /> */}
          </TabPanel>
        </TabContext>
        <div
          style={{
            display: "flex",
            marginTop: 20,
            justifyContent: "space-around",
            width: "100%",
          }}
        >
          <div>
            <FormControl>
              <FormLabel id="demo-controlled-radio-buttons-group">
                TimeLine
              </FormLabel>
              <RadioGroup
                aria-labelledby="demo-controlled-radio-buttons-group"
                name="controlled-radio-buttons-group"
                value={days}
                onChange={handleChange}
              >
                <FormControlLabel value="1w" control={<Radio />} label="1w" />
                <FormControlLabel value="1m" control={<Radio />} label="1m" />
                <FormControlLabel value="3m" control={<Radio />} label="3m" />
                <FormControlLabel value="6m" control={<Radio />} label="6m" />
                <FormControlLabel value="1y" control={<Radio />} label="1y" />
                <FormControlLabel value="all" control={<Radio />} label="all" />
              </RadioGroup>
            </FormControl>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ChartPage;
